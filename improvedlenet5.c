#include "improvedlenet5.h"
#include "lenet5params.h"
#include <stdio.h>
/*
	各层参数
	net=torch.nn.Sequential(
	nn.Conv2d(3,6,kernel_size=5,padding=2),nn.ReLU(),
	nn.MaxPool2d(kernel_size=2,stride=2),
	nn.Conv2d(6,16,kernel_size=5),nn.ReLU(),
	nn.MaxPool2d(kernel_size=2,stride=2),nn.Flatten(),
	nn.Linear(576,120),nn.ReLU(),
	nn.Linear(120,84),nn.ReLU(),
	nn.Linear(84,2)
	各层输出
	Conv2d output shape:	 torch.Size([1, 6, 32, 32])
	ReLU output shape:		 torch.Size([1, 6, 32, 32])
	MaxPool2d output shape:	 torch.Size([1, 6, 16, 16])
	Conv2d output shape:	 torch.Size([1, 16, 12, 12])
	ReLU output shape:		 torch.Size([1, 16, 12, 12])
	MaxPool2d output shape:	 torch.Size([1, 16, 6, 6])
	Flatten output shape:	 torch.Size([1, 576])
	Linear output shape:	 torch.Size([1, 120])
	ReLU output shape:		 torch.Size([1, 120])
	Linear output shape:	 torch.Size([1, 84])
	ReLU output shape:		 torch.Size([1, 84])
	Linear output shape:	 torch.Size([1, 2])

*/

extern float conv1Weight[6][3][5][5];
extern float conv2Weight[16][6][5][5];
extern float fullConnect1Weight[120][576];
extern float fullConnect2Weight[84][120];
extern float fullConnect3Weight[2][84];

extern float fullConnect1Bias[120];
extern float fullConnect2Bias[84];
extern float fullConnect3Bias[2];

static void mallocForppptr(float*** imageSpace,imageSize spaceSize) 
{
	for (int k = 0; k < spaceSize.dimsc; k++)
	{
		imageSpace[k] = (float**)malloc(spaceSize.rowsc * sizeof(float*));
		while(imageSpace[k] == NULL){
			printf("imageSpace[i] malloc failed\n");
			imageSpace[k] = (float**)malloc(spaceSize.rowsc * sizeof(float*));
		}	
	}
	for (int i = 0; i < spaceSize.dimsc; i++)
		for(int j=0;j<spaceSize.rowsc;j++){
			imageSpace[i][j] = (float*)malloc(spaceSize.colsc * sizeof(float));
			while(imageSpace == NULL){
				printf("imageSpace[j] malloc failed\n");
				imageSpace[i][j] = (float*)malloc(spaceSize.colsc * sizeof(float));
			}
		}
	
}
static void freeForppptr(float*** imageSpace,imageSize spaceSize) 
{
	// Optimized your memory allocation code
	for (int k = 0; k < spaceSize.dimsc; k++)
	{
		for (int i = 0; i < spaceSize.rowsc; i++)
		{
			free((void*)imageSpace[k][i]);	
		}
		free((void*)imageSpace[k]);
	}

}

/*
	@params
		input: the input matrix of lenet5 
		inputSize: a imageSize of the input matrix (type float****)  
	@retval 
		res: the classify result, using the number of node in ouput layer
*/
int lenet5Improved(float*** input,imageSize inputSize)
{
	
	imageSize OutSize1 = { 1,6,32,32 };
	float*** Out1 = (float***)malloc(OutSize1.dimsc*sizeof(float**));
	mallocForppptr(Out1,OutSize1);
	
	imageSize kernelSize1 = { 6,3,5,5 };
	float**** kernel1 = (float****)malloc(kernelSize1.numsc*sizeof(float***));
	for(int n=0;n<kernelSize1.numsc;n++){
		kernel1[n] = (float***)malloc(kernelSize1.rowsc*sizeof(float**));
		for(int d=0;d<kernelSize1.dimsc;d++){
			kernel1[n][d] = (float**)malloc(kernelSize1.rowsc*sizeof(float*));
			for(int i=0;i<kernelSize1.rowsc;i++){
				kernel1[n][d][i] = (float*)malloc(kernelSize1.colsc*sizeof(float));
				for(int j = 0;j<kernelSize1.colsc;j++){
					kernel1[n][d][i][j] =  conv1Weight[n][d][i][j];		
				}
			}
		}		
	}

	int padding1 = 2;
	int step1 = 1;
	//第一层 卷积处理
	cnnOperationConvolution(input, inputSize,Out1, OutSize1, kernel1, kernelSize1,padding1,step1);
	//激活函数
	cnnOperationActivation((float***)Out1, OutSize1, 0);

	//第二层 池化处理
	imageSize OutSize2 = { 1,6,16,16 };
	float*** Out2 = (float***)malloc(OutSize2.dimsc*sizeof(float**));
	mallocForppptr(Out2,OutSize2);
	imageSize kernelSize2 = { 1,1,2,2 };
	float**** kernel2 = (float****)malloc(kernelSize2.numsc*sizeof(float***));
	
	int padding2 = 0;
	int step2 = 2;
	cnnOperationPooling(Out1, OutSize1,Out2, OutSize2, kernelSize2, padding2,step2);
	freeForppptr(Out1,OutSize1);
	//第三层 卷积处理
	imageSize OutSize3 = { 1,16,12,12 };
	float*** Out3 = (float***)malloc(OutSize3.dimsc*sizeof(float**)); 
	mallocForppptr(Out3, OutSize3);
	imageSize kernelSize3 = { 16,6,5,5 };
	float**** kernel3 = (float****)malloc(kernelSize3.numsc*sizeof(float***));
	for(int n=0;n<kernelSize3.numsc;n++){
		kernel3[n] = (float***)malloc(kernelSize3.rowsc*sizeof(float**));
		for(int d=0;d<kernelSize3.dimsc;d++){
			kernel3[n][d] = (float**)malloc(kernelSize3.rowsc*sizeof(float*));
			for(int i=0;i<kernelSize3.rowsc;i++){
				kernel3[n][d][i] = (float*)malloc(kernelSize3.colsc*sizeof(float));
				for(int j = 0;j<kernelSize3.colsc;j++){
					kernel3[n][d][i][j] =  conv2Weight[n][d][i][j];			
				}
			}
		}		
	}
	int padding3 = 0;
	int step3 = 1;
	cnnOperationConvolution(Out2, OutSize2, Out3, OutSize3, kernel3, kernelSize3, padding3, step3);
	freeForppptr(Out2,OutSize2);
	//激活函数
	cnnOperationActivation((float***)Out3, OutSize3, 0);
	//第四层 池化处理
	imageSize OutSize4 = { 16,1,6,6 };
	float*** Out4 = (float***)malloc(OutSize4.dimsc*sizeof(float**)); 
	mallocForppptr(Out4, OutSize4);
	imageSize kernelSize4 = {1,1,2,2};
	float**** kernel4 = NULL;
	int padding4 = 0;
	int step4 = 2;
	cnnOperationPooling(Out3, OutSize3, Out4, OutSize4,kernelSize4, padding4,step4);
	freeForppptr(Out3,OutSize3);
	//第五层 扁平化

	imageSize OutSize5 = { 1,1,1,576 };
	float*** Out5 = (float***)malloc(OutSize5.dimsc*sizeof(float**)); 
	mallocForppptr(Out5, OutSize5);
	cnnOperationFlatten(Out4, OutSize4, Out5, OutSize5);
	freeForppptr(Out4,OutSize4);
	//第六层 全连接
	imageSize OutSize6 = { 1,1,1,120 };
	float*** Out6 = (float***)malloc(OutSize6.dimsc*sizeof(float**)); 
	mallocForppptr(Out6, OutSize6);
	float** weight6 = (float**)malloc(OutSize6.colsc*sizeof(float*)); 
	for(int i=0;i<OutSize6.colsc;i++){
		weight6[i] = (float*)malloc(OutSize5.colsc*sizeof(float));
		for(int j=0;j<OutSize5.colsc;j++)
			weight6[i][j] = fullConnect1Weight[i][j];
	}
	float* bias6 = (float*)malloc(OutSize6.colsc*sizeof(float)); 
	for(int i=0;i<OutSize6.colsc;i++){
		bias6[i] = fullConnect1Bias[i];
	} 
	
	cnnOperationLinear(Out5, OutSize5, Out6, OutSize6,weight6,bias6);
	freeForppptr(Out5,OutSize5);
	//激活函数
	cnnOperationActivation((float***)Out6, OutSize6, 0);
	//第七层 全连接

	imageSize OutSize7 = { 1,1,1,84 };
	float*** Out7 = (float***)malloc(OutSize7.dimsc*sizeof(float**)); 
	mallocForppptr(Out7, OutSize7);
	float** weight7 = (float**)malloc(OutSize7.colsc*sizeof(float*)); 
	for(int i=0;i<OutSize7.colsc;i++){
		weight7[i] = (float*)malloc(OutSize6.colsc*sizeof(float));
		for(int j=0;j<OutSize6.colsc;j++)
			weight7[i][j] = fullConnect2Weight[i][j];
	}
	float* bias7 = (float*)malloc(OutSize7.colsc*sizeof(float)); 
	for(int i=0;i<OutSize7.colsc;i++){
		bias7[i] = fullConnect1Bias[i];
	} 

	cnnOperationLinear(Out6, OutSize6, Out7, OutSize7,weight7,bias7);
	freeForppptr(Out6,OutSize6);
	//激活函数
	cnnOperationActivation((float***)Out7, OutSize7, 0);
	//第八层 全连接
	imageSize OutSize8 = { 1,1,1,2 };
	float*** Out8 = (float***)malloc(OutSize8.dimsc*sizeof(float**)); 
	mallocForppptr(Out8, OutSize8);
	float** weight8 = (float**)malloc(OutSize8.colsc*sizeof(float*)); 
	for(int i=0;i<OutSize8.colsc;i++){
		weight8[i] = (float*)malloc(OutSize7.colsc*sizeof(float));
		for(int j=0;j<OutSize7.colsc;j++)
			weight8[i][j] = fullConnect1Weight[i][j];
	}
	float* bias8 = (float*)malloc(OutSize8.colsc*sizeof(float)); 
	for(int i=0;i<OutSize8.colsc;i++){
		bias8[i] = fullConnect1Bias[i];
	} 

	cnnOperationLinear(Out7, OutSize7, Out8, OutSize8, weight8,bias8);
	freeForppptr(Out7,OutSize7);

	int *resptr = (int*)malloc(sizeof(int));

	matOperationMaxIt(Out8[0],OutSize8,resptr);
	printf("out8[0][0][0]=%f out8[0][0][1]=%f\n",Out8[0][0][0],Out8[0][0][1]);
	int res = (Out8[0][0][0]>Out8[0][0][1])?Out8[0][0][0]:Out8[0][0][1];
	
	freeForppptr(Out8,OutSize8);;
	
	return res;
}








